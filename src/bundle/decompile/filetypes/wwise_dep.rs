// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::io::{Cursor, Read};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::Result;

use crate::bundle::UserFile;

#[tracing::instrument(skip(binary), fields(binary = format!("&[u8; {}]", binary.as_ref().len())))]
pub fn decompile(binary: impl AsRef<[u8]>) -> Result<Vec<UserFile>> {
    let mut c = Cursor::new(binary);

    let version = read32!(c)?;
    let length = read32!(c)? as usize;

    tracing::debug!(version, length);

    // The string is zero-terminated, so we ignore the last character
    let mut buf = vec![0u8; length - 1];
    c.read_exact(&mut buf)?;

    Ok(vec![UserFile::new(buf)])
}
