// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::collections::HashMap;
use std::io::{Cursor, Read, Seek};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::eyre;
use color_eyre::Result;

use crate::bundle::{BundleFileType, UserFile};
use crate::context::Context;
use crate::murmur::HashGroup;

#[inline]
fn read_hash<T>(ctx: &Context, c: &mut T) -> Result<String>
where
    T: Read + Seek,
{
    let hash = read64!(c)?;
    let name = ctx
        .dictionary
        .lookup(hash.into(), HashGroup::Filename)
        .cloned()
        .unwrap_or_else(|| format!("{:016X}", hash));
    Ok(name)
}

#[tracing::instrument(skip(ctx, binary))]
pub fn decompile(ctx: &Context, binary: impl AsRef<[u8]>) -> Result<Vec<UserFile>> {
    let mut c = Cursor::new(binary);

    let file_count = read32!(c)?;
    let mut file_map: HashMap<BundleFileType, Vec<String>> = HashMap::new();

    for _ in 0..file_count {
        let t = BundleFileType::from(read64!(c)?);
        let name = read_hash(ctx, &mut c)?;

        let list = {
            file_map.entry(t).or_default();

            file_map.get_mut(&t).ok_or_else(|| {
                eyre!(
                    "Couldn't get file list for type {}.",
                    t.decompiled_ext_name()
                )
            })?
        };

        list.push(name);
    }

    let mut s = String::new();

    for (key, val) in file_map.iter() {
        s.push_str(&format!("{} = [", key.ext_name()));
        s.push_str(&val.iter().fold(String::new(), |mut s, name| {
            s.push_str("\n\"");
            s.push_str(name);
            s.push('"');
            s
        }));
        s.push_str("\n]\n");
    }

    Ok(vec![UserFile::new(s.into_bytes())])
}
