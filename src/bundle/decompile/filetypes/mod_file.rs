// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use color_eyre::eyre::ensure;
use color_eyre::Result;

use crate::bundle::UserFile;

#[tracing::instrument(skip(data))]
pub fn decompile(data: impl AsRef<[u8]>) -> Result<Vec<UserFile>> {
    let data = data.as_ref();
    let length = data[..4].iter().rev().fold(0u64, |l, &i| l << 8 | i as u64) as usize;

    let content = &data[16..];
    ensure!(
        length == content.len(),
        "expected to read {} bytes, got {} bytes",
        length,
        content.len()
    );

    Ok(vec![UserFile::new(content.to_vec())])
}
