// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::env;
use std::fs;
use std::io::{Cursor, Read, Seek, SeekFrom};
use std::process::Command;

use byteorder::ReadBytesExt;
use color_eyre::eyre::ensure;
use color_eyre::eyre::Context as _;
use color_eyre::Result;

use crate::bundle::UserFile;
use crate::context::Context;

// Implementation based on https://en.wikipedia.org/wiki/LEB128
fn read_uleb128<T>(c: &mut T) -> Result<u64>
where
    T: Read + Seek,
{
    let mut result: u64 = 0;
    let mut shift: u64 = 0;

    loop {
        let byte = read8!(c)? as u64;
        result |= (byte & 0x7f) << shift;

        if byte < 0x80 {
            return Ok(result);
        }

        shift += 7;
    }
}

#[tracing::instrument(skip(ctx, binary))]
pub fn decompile(ctx: &Context, binary: impl AsRef<[u8]>) -> Result<Vec<UserFile>> {
    let binary = binary.as_ref();
    let length = binary[..4]
        .iter()
        .rev()
        .fold(0u64, |l, &i| l << 8 | i as u64) as usize;

    let content = &binary[12..];
    ensure!(
        length == content.len(),
        "expected to read {} bytes, got {} bytes",
        length,
        content.len()
    );

    let name = {
        let mut c = Cursor::new(content);

        // Skip magic header (3 bytes) and version (1 byte)
        c.seek(SeekFrom::Current(4))?;
        // Skip additional header byte(s)
        read_uleb128(&mut c)?;
        let length = read_uleb128(&mut c)? as usize;

        let mut buf = vec![0u8; length];
        c.read_exact(&mut buf)?;
        let mut s = String::from_utf8(buf)?;
        // Remove the leading `@`
        s.remove(0);
        s
    };

    let mut temp = env::temp_dir();
    // Flatten file name, so we don't have to deal with creating and removing directories
    temp.push(name.replace('/', "_"));
    temp.set_extension("luao");

    tracing::trace!("Writing temporary lua object file to '{}'", temp.display());

    fs::write(&temp, content)?;

    let mut cmd = ctx
        .ljd
        .as_ref()
        .map(From::from)
        .unwrap_or_else(|| Command::new("ljd"));

    cmd.arg("-f")
        .arg(&temp)
        .arg("--catch_asserts")
        .args(["--unsafe", "false"])
        .args(["--function_def_sugar", "false"])
        .args(["--function_def_self_arg", "true"]);

    tracing::debug!("Executing command: {:?}", cmd);

    let content = cmd
        .output()
        .wrap_err("failed to decompile with ljd")?
        .stdout;

    fs::remove_file(&temp)?;

    tracing::trace!("Removed temporary file '{}'", temp.display());

    Ok(vec![UserFile::with_name(content, name)])
}
