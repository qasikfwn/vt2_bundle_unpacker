// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::convert::From;
use std::fmt;
use std::io::{Cursor, Seek, SeekFrom};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::{bail, ensure};
use color_eyre::Result;

use super::super::wem::WEMFile;

#[derive(Copy, Clone)]
enum HIRCObjectType {
    Setting,
    SoundSFX,
    EventAction,
    Event,
    RandomContainer,
    SwitchContainer,
    ActorMixer,
    AudioBus,
    BlendContainer,
    MusicSegment,
    MusicTrack,
    MusicSwitchContainer,
    MusicPlaylistContainer,
    Attenuation,
    DialogEvent,
    MotionBus,
    MotionFX,
    Effect,
    AuxiliaryBus,
    Unknown(u8),
}

impl fmt::Display for HIRCObjectType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Setting => write!(f, "Setting"),
            Self::SoundSFX => write!(f, "SoundSFX"),
            Self::EventAction => write!(f, "EventAction"),
            Self::Event => write!(f, "Event"),
            Self::RandomContainer => write!(f, "RandomContainer"),
            Self::SwitchContainer => write!(f, "SwitchContainer"),
            Self::ActorMixer => write!(f, "ActorMixer"),
            Self::AudioBus => write!(f, "AudioBus"),
            Self::BlendContainer => write!(f, "BlendContainer"),
            Self::MusicSegment => write!(f, "MusicSegment"),
            Self::MusicTrack => write!(f, "MusicTrack"),
            Self::MusicSwitchContainer => write!(f, "MusicSwitchContainer"),
            Self::MusicPlaylistContainer => write!(f, "MusicPlaylistContainer"),
            Self::Attenuation => write!(f, "Attenuation"),
            Self::DialogEvent => write!(f, "DialogEvent"),
            Self::MotionBus => write!(f, "MotionBus"),
            Self::MotionFX => write!(f, "MotionFX"),
            Self::Effect => write!(f, "Effect"),
            Self::AuxiliaryBus => write!(f, "AuxiliaryBus"),
            Self::Unknown(t) => write!(f, "Unknown({:02X})", t),
        }
    }
}

impl From<u8> for HIRCObjectType {
    fn from(t: u8) -> Self {
        match t {
            1 => Self::Setting,
            2 => Self::SoundSFX,
            3 => Self::EventAction,
            4 => Self::Event,
            5 => Self::RandomContainer,
            6 => Self::SwitchContainer,
            7 => Self::ActorMixer,
            8 => Self::AudioBus,
            9 => Self::BlendContainer,
            10 => Self::MusicSegment,
            11 => Self::MusicTrack,
            12 => Self::MusicSwitchContainer,
            13 => Self::MusicPlaylistContainer,
            14 => Self::Attenuation,
            15 => Self::DialogEvent,
            16 => Self::MotionBus,
            17 => Self::MotionFX,
            18 => Self::Effect,
            20 => Self::AuxiliaryBus,
            _ => Self::Unknown(t),
        }
    }
}

impl From<HIRCObjectType> for u8 {
    fn from(t: HIRCObjectType) -> u8 {
        match t {
            HIRCObjectType::Setting => 1,
            HIRCObjectType::SoundSFX => 2,
            HIRCObjectType::EventAction => 3,
            HIRCObjectType::Event => 4,
            HIRCObjectType::RandomContainer => 5,
            HIRCObjectType::SwitchContainer => 6,
            HIRCObjectType::ActorMixer => 7,
            HIRCObjectType::AudioBus => 8,
            HIRCObjectType::BlendContainer => 9,
            HIRCObjectType::MusicSegment => 10,
            HIRCObjectType::MusicTrack => 11,
            HIRCObjectType::MusicSwitchContainer => 12,
            HIRCObjectType::MusicPlaylistContainer => 13,
            HIRCObjectType::Attenuation => 14,
            HIRCObjectType::DialogEvent => 15,
            HIRCObjectType::MotionBus => 16,
            HIRCObjectType::MotionFX => 17,
            HIRCObjectType::Effect => 18,
            HIRCObjectType::AuxiliaryBus => 20,
            HIRCObjectType::Unknown(t) => t,
        }
    }
}

#[tracing::instrument(skip(content))]
pub fn extract_hirc_files(content: impl AsRef<[u8]>) -> Result<Vec<WEMFile>> {
    let mut c = Cursor::new(content.as_ref());

    let object_count = read32!(c)? as usize;

    for _ in 0..object_count {
        let t = HIRCObjectType::from(read8!(c)?);
        let length = read32!(c)? as usize;
        let pos = c.position();

        let id = read32!(c)?;
        tracing::trace!(
            "Found HIRC Section '{:08X}' of type {} with length {} at {}",
            id,
            t,
            length,
            pos
        );

        match t {
            HIRCObjectType::SoundSFX => {
                // Skip unknown bytes
                c.seek(SeekFrom::Current(4))?;

                // Technically this field has three values, but we only care about whether it's embedded
                let is_embedded = read32!(c)? == 0;
                ensure!(
                    !is_embedded,
                    "SoundSFX file is embedded. Don't know how to handle this"
                );

                let _audio_id = read32!(c)?;

                c.seek(SeekFrom::Start(pos + length as u64))?;
            }
            HIRCObjectType::MusicTrack => {
                bail!("Found a MusicTrack, don't know how to handle this one.")
            }
            HIRCObjectType::MusicSegment => {
                bail!("Found a MusicSegment, don't know how to handle this one.")
            }
            _ => {
                // Skip objects that have no value to us
                if length > 4 {
                    c.seek(SeekFrom::Current((length - 4) as i64))?;
                }
            }
        }
    }

    Ok(Vec::new())
}
