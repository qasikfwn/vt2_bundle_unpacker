// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::io::{Cursor, Read, Seek, SeekFrom};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::bail;
use color_eyre::Result;

use super::super::wem::WEMFile;

pub const DIDX_HEADER: u32 = 0x58444944;
pub const DATA_HEADER: u32 = 0x41544144;

macro_rules! check_header {
    ($c:expr, $h:expr) => {{
        let header = read32!($c)?;
        if header != $h {
            bail!(
                "Unexpected header. Expected {:08X}, got {:08X}. At position {}",
                $h,
                header,
                $c.position()
            );
        }
    }};
}

struct WEMFileIndex {
    id: u32,
    offset: u32,
    length: usize,
}

#[tracing::instrument(skip(didx_content, data_content))]
pub fn extract_data_files(
    didx_content: impl AsRef<[u8]>,
    data_content: impl AsRef<[u8]>,
) -> Result<Vec<WEMFile>> {
    let mut didx_cursor = Cursor::new(didx_content);
    let mut data_cursor = Cursor::new(data_content);

    let index: Vec<WEMFileIndex> = {
        check_header!(didx_cursor, DIDX_HEADER);

        // Store the length and start of the section data, so we can explicitely
        // seek to the end of it, once we've finished reading
        let section_length = read32!(didx_cursor)? as usize;

        // Each entry in the index holds three u32 values -> 12 bytes
        let file_count = section_length / 12;

        let mut index: Vec<WEMFileIndex> = Vec::with_capacity(file_count);

        for _ in 0..file_count {
            index.push(WEMFileIndex {
                id: read32!(didx_cursor)?,
                offset: read32!(didx_cursor)?,
                length: read32!(didx_cursor)? as usize,
            });
        }

        index
    };

    let files: Vec<WEMFile> = {
        check_header!(data_cursor, DATA_HEADER);

        // Skip the section length. We don't need it.
        data_cursor.seek(SeekFrom::Current(4))?;
        let start_of_section_data = data_cursor.position();

        let mut files: Vec<WEMFile> = Vec::with_capacity(index.len());

        for i in index {
            // Explicitely seek to start of file, so we don't have to care
            // if we're iterating out of order
            data_cursor.seek(SeekFrom::Start(start_of_section_data + i.offset as u64))?;
            let mut buf: Vec<u8> = vec![0u8; i.length];
            data_cursor.read_exact(&mut buf)?;
            files.push(WEMFile::new(i.id, buf));
        }

        files
    };

    Ok(files)
}
