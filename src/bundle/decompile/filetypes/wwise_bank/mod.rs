// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::io::{Cursor, Read};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::{bail, Context as _};
use color_eyre::Result;

use crate::bundle::UserFile;

mod bnk;
mod wem;

#[tracing::instrument(skip(binary))]
pub fn read_bnk(binary: impl AsRef<[u8]>) -> Result<Vec<u8>> {
    let mut c = Cursor::new(binary);

    let version = read32!(c)?;

    if version != 5 {
        bail!(
            "Don't know how to handle files with version number {}",
            version
        );
    }

    let length = read32!(c)? as usize;
    let _name_hash = read64!(c)?;

    let mut buf = Vec::new();
    let len = c.read_to_end(&mut buf)?;

    if len != length {
        bail!(
            "Didn't read expected number of bytes. Read {}, expected {}",
            len,
            length
        );
    }

    Ok(buf)
}

#[tracing::instrument(skip(binary))]
pub fn decompile(binary: impl AsRef<[u8]>) -> Result<Vec<UserFile>> {
    let bnk_content = read_bnk(binary).wrap_err("Failed to read bnk content")?;
    let wem_files = bnk::extract_files(bnk_content).wrap_err("Failed to extract bnk files")?;

    wem_files
        .iter()
        .map(|file| {
            file.to_wav()
                .map(|bin| UserFile::with_name(bin, format!("{}.wav", file.get_name())))
                .wrap_err_with(|| format!("Failed to convert '{}' to `.wav`", file.get_name()))
        })
        .collect::<Result<Vec<UserFile>>>()
}
