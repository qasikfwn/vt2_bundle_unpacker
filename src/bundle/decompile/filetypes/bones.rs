use std::io::{BufRead as _, Cursor, Seek, SeekFrom};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::{bail, Context as _};
use color_eyre::Result;
use serde::Serialize;

use crate::bundle::UserFile;

#[derive(Serialize)]
struct BonesResource {
    bones: Vec<String>,
    lod_levels: Vec<u32>,
}

#[tracing::instrument(skip(binary))]
pub fn decompile(binary: impl AsRef<[u8]>) -> Result<Vec<UserFile>> {
    let mut csr = Cursor::new(binary);

    let bone_count = read32!(csr)?;
    let lod_count = read32!(csr)?;

    let mut bones_res = BonesResource {
        bones: Vec::with_capacity(bone_count as usize),
        lod_levels: Vec::with_capacity(lod_count as usize),
    };

    // Next part of file is the short hashes of the bone names
    // We can skip over these, as the clear names are provided later
    // in the file as well.
    csr.seek(SeekFrom::Current((bone_count * 4) as i64))?;

    // LOD levels are just a u32 array
    for _ in 0..lod_count {
        bones_res.lod_levels.push(read32!(csr)?);
    }

    // Some .bones files contain 0 bones, 1 LOD level, and are only 12 bytes long
    // The decompiled result will just be:
    // ```sjson
    // bones = []
    // lod_levels = [0]
    // ````

    // Bone names are stored as an array of null-terminated strings
    for _ in 0..bone_count {
        let mut buf: Vec<u8> = Vec::with_capacity(64);
        csr.read_until(0, &mut buf)?;

        // `read_until` includes the delimiter, so we need to remove
        // the null-terminator here.
        buf.pop();

        let s = String::from_utf8(buf).wrap_err("Invalid UTF-8 in bone name")?;
        bones_res.bones.push(s);
    }

    let pos = csr.position() as usize;
    let len = csr.into_inner().as_ref().len();

    if pos < len {
        bail!("Failed to consume all data, {} bytes remaining", len - pos);
    }

    serde_sjson::to_vec(&bones_res)
        .wrap_err("Failed to serialize bones data")
        .map(UserFile::new)
        .map(|f| vec![f])
}
