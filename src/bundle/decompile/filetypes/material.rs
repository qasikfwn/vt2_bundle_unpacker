// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::io::{Cursor, Read, Seek, SeekFrom};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::{bail, Context as _};
use color_eyre::{Report, Result};
use serde::Serialize;

use crate::bundle::UserFile;
use crate::context::Context;
use crate::murmur::{HashGroup, MurmurBuildHasher};

const EXPECTED_MATERIAL_VERSION: u32 = 43;

type HashMap<K, V> = std::collections::HashMap<K, V, MurmurBuildHasher>;

// VariableType should be serialized as a lowercase string
#[derive(Clone, Copy, Debug, Serialize)]
#[serde(rename_all(serialize = "lowercase"))]
enum VariableType {
    Scalar,
    Vector2,
    Vector3,
    Vector4,
}

impl TryFrom<u32> for VariableType {
    type Error = Report;
    fn try_from(value: u32) -> Result<Self> {
        match value {
            0 => Ok(Self::Scalar),
            1 => Ok(Self::Vector2),
            2 => Ok(Self::Vector3),
            3 => Ok(Self::Vector4),
            _ => bail!("Unknown VariableType {}", value),
        }
    }
}

// Variable values can be a single f32 or vec of 2-4 f32s
#[derive(Clone, Debug, Serialize)]
#[serde(tag = "type", rename_all = "lowercase")]
enum ShaderVariable {
    Scalar { value: f32 },
    Vector2 { value: [f32; 2] },
    Vector3 { value: [f32; 3] },
    Vector4 { value: [f32; 4] },
}

#[derive(Clone, Debug)]
struct VariableReflection {
    variable_type: VariableType,
    _elements: u32,
    name: String,
    offset: u32,
    _element_stride: u32,
}

impl VariableReflection {
    #[tracing::instrument(skip(c))]
    fn try_get_variable<T>(
        &self,
        c: &mut Cursor<T>,
        variable_data_start: u64,
    ) -> Result<ShaderVariable>
    where
        T: AsRef<[u8]>,
    {
        // Move cursor to correct offset within the material's `variable_data`
        c.seek(SeekFrom::Start(variable_data_start + self.offset as u64))?;

        match self.variable_type {
            VariableType::Scalar => {
                let v = readf32!(c)?;
                Ok(ShaderVariable::Scalar { value: v })
            }
            VariableType::Vector2 => {
                let v = [readf32!(c)?, readf32!(c)?];
                Ok(ShaderVariable::Vector2 { value: v })
            }
            VariableType::Vector3 => {
                let v = [readf32!(c)?, readf32!(c)?, readf32!(c)?];
                Ok(ShaderVariable::Vector3 { value: v })
            }
            VariableType::Vector4 => {
                let v = [readf32!(c)?, readf32!(c)?, readf32!(c)?, readf32!(c)?];
                Ok(ShaderVariable::Vector4 { value: v })
            }
        }
    }
}

#[derive(Serialize)]
struct MaterialTemplate {
    #[serde(skip_serializing_if = "Option::is_none")]
    shader: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    parent_material: Option<String>,
    #[serde(skip_serializing_if = "HashMap::is_empty")]
    material_contexts: HashMap<String, String>,
    #[serde(skip_serializing_if = "HashMap::is_empty")]
    textures: HashMap<String, String>,
    #[serde(skip_serializing_if = "HashMap::is_empty")]
    variables: HashMap<String, ShaderVariable>,
}

#[derive(Serialize)]
#[serde(untagged)]
enum MaterialResource {
    Single(MaterialTemplate),
    Set(HashMap<String, MaterialTemplate>),
}

#[inline]
fn read_short_hash(ctx: &Context, c: &mut impl Read, group: HashGroup) -> Result<Option<String>> {
    let hash = read32!(c)?;
    // A hash of `0x0` is an empty string (as long as the murmur hash seed is `0x0`)
    if hash == 0 {
        return Ok(None);
    }
    let name = ctx
        .dictionary
        .lookup_short(hash.into(), group)
        .cloned()
        .unwrap_or_else(|| format!("{:08X}", hash));
    Ok(Some(name))
}

#[inline]
fn read_hash(ctx: &Context, c: &mut impl Read, group: HashGroup) -> Result<Option<String>> {
    let hash = read64!(c)?;
    if hash == 0 {
        return Ok(None);
    }
    let name = ctx
        .dictionary
        .lookup(hash.into(), group)
        .cloned()
        .unwrap_or_else(|| format!("{:016X}", hash));
    Ok(Some(name))
}

#[tracing::instrument(skip(ctx, c))]
fn read_texture_channel_array(ctx: &Context, c: &mut impl Read) -> Result<HashMap<String, String>> {
    // Array of <ShortHash, Hash>
    let mut result: HashMap<String, String> = HashMap::default();
    let count = read32!(c)?;

    for _ in 0..count {
        let channel = read_short_hash(ctx, c, HashGroup::Material)?.unwrap_or_default();
        let name = read_hash(ctx, c, HashGroup::Filename)?.unwrap_or_default();
        result.insert(channel, name);
    }

    Ok(result)
}

#[tracing::instrument(skip(ctx, c))]
fn read_material_context_array(
    ctx: &Context,
    c: &mut impl Read,
) -> Result<HashMap<String, String>> {
    //Array of <ShortHash, ShortHash>
    let mut result: HashMap<String, String> = HashMap::default();
    let count = read32!(c)?;

    for _ in 0..count {
        let context = read_short_hash(ctx, c, HashGroup::Material)?.unwrap_or_default();
        let material = read_short_hash(ctx, c, HashGroup::Filename)?.unwrap_or_default();
        result.insert(context, material);
    }

    Ok(result)
}

#[tracing::instrument(skip(ctx, c))]
fn read_variable_reflection_array(
    ctx: &Context,
    c: &mut impl Read,
) -> Result<Vec<VariableReflection>> {
    let mut result: Vec<VariableReflection> = Vec::new();
    let count = read32!(c)?;

    for _ in 0..count {
        let variable_type = VariableType::try_from(read32!(c)?)?;
        let elements = read32!(c)?;
        let name = read_short_hash(ctx, c, HashGroup::Material)?.unwrap_or_default();
        let offset = read32!(c)?;
        let element_stride = read32!(c)?;

        // All shader variables in VT2 materials appear to have `elements = 0` and `element_stride = 0`
        if elements != 0 || element_stride != 0 {
            bail!(
                "Not implemented: shader variable elements = {}, element_stride = {}",
                elements,
                element_stride
            )
        }

        let variable = VariableReflection {
            variable_type,
            _elements: elements,
            name,
            offset,
            _element_stride: element_stride,
        };
        result.push(variable);
    }

    Ok(result)
}

#[tracing::instrument(skip(ctx, c))]
fn decompile_material<T>(ctx: &Context, c: &mut Cursor<T>) -> Result<MaterialTemplate>
where
    T: AsRef<[u8]>,
{
    // A hash of `0x0` means an empty string
    let shader = read_short_hash(ctx, c, HashGroup::Material)?;
    let parent_material = read_hash(ctx, c, HashGroup::Filename)?;

    let textures = read_texture_channel_array(ctx, c)?;
    let material_contexts = read_material_context_array(ctx, c)?;
    let variable_reflection = read_variable_reflection_array(ctx, c)?;

    let variable_data_len = read32!(c)?;

    let variable_data_start: u64 = c.position();

    // Shader variable values are stored together in a byte array
    // Each `VariableReflection` stores the offset of its variable's data within the byte array
    let variables: HashMap<String, ShaderVariable> =
        variable_reflection
            .into_iter()
            .try_fold(HashMap::default(), |mut map, variable| {
                let value = variable.try_get_variable(c, variable_data_start)?;
                map.insert(variable.name, value);
                Ok::<_, Report>(map)
            })?;

    // This material might be part of a set:
    // move the cursor to the end of the current material data
    let variable_data_end = variable_data_start + variable_data_len as u64;
    c.seek(SeekFrom::Start(variable_data_end))?;

    Ok(MaterialTemplate {
        shader,
        parent_material,
        material_contexts,
        textures,
        variables,
    })
}

#[tracing::instrument(skip(ctx, c))]
fn decompile_material_set<T>(
    ctx: &Context,
    c: &mut Cursor<T>,
) -> Result<HashMap<String, MaterialTemplate>>
where
    T: AsRef<[u8]>,
{
    // Material sets are just an array of <ShortHash, MaterialTemplate>
    let mut result: HashMap<String, MaterialTemplate> = HashMap::default();
    let count = read32!(c)?;

    for _ in 0..count {
        let first = read_short_hash(ctx, c, HashGroup::Material)?.unwrap_or_default();
        let second = decompile_material(ctx, c)?;
        result.insert(first, second);
    }

    Ok(result)
}

#[tracing::instrument(skip(ctx, binary))]
pub fn decompile(
    ctx: &Context,
    binary: impl AsRef<[u8]>,
    decompiled_name: String,
) -> Result<Vec<UserFile>> {
    let mut c = Cursor::new(binary);

    let material_version = read32!(c)?;
    if material_version != EXPECTED_MATERIAL_VERSION {
        bail!(
            "Wrong material version. Got {}, expected {}",
            material_version,
            EXPECTED_MATERIAL_VERSION
        );
    }

    // Flag is set to `1`, when the file contains only a single, unnamed material definition
    // `0` when the file contains a material set
    let is_single = match read32!(c)? {
        0 => false,
        1 => true,
        v => {
            bail!("Unexpected value for material type: {}", v);
        }
    };

    // Offset within the file at which actual material data starts
    let material_offset = read32!(c)?;
    // Length of material data
    let _material_size = read32!(c)?;

    // `0xffffffff` when there are no shader nodes
    // Offset into file otherwise
    let shader_offset = read32!(c)?;
    let has_shader_nodes = shader_offset != 0xffffffffu32;
    // `0x0` when there are no shader nodes
    // Length of shader data otherwise
    let _shader_size = read32!(c)?;

    // A material can get its shader from:
    //    - an embedded shader (shader nodes)
    //    - naming a shader (uber shader)
    //    - inheriting from the parent material
    // Stingray does not support material sets with embedded shaders
    if !is_single && has_shader_nodes {
        bail!("Material sets with embedded shaders not implemented.");
    }

    // It appears that the original node graph data for an embedded shader is not
    // present in compiled material files. The only way to get that data would be to
    // figure out some method of decompiling and reverse engineering the shader binary
    // (complicated!)
    // The best we can do for now is decompile the material anyway and include `shader = {}`
    // in the SJSON to indicate to the user that there would have been shader nodes.
    if has_shader_nodes {
        tracing::warn!("{} has shader nodes; decompiling anyway", decompiled_name);
    }

    // Material data always seems to be directly after header (0x24), but seek just in case
    if material_offset != 0x18 {
        tracing::warn!("Unusual material data offset: {}", material_offset);
        c.seek(SeekFrom::Start(material_offset.into()))?;
    }

    // If the material has an embedded shader, another resource file is appended to the material binary
    // Call it `<material resource name>.shader`
    let shader_file_name = {
        let mut out = String::new();
        let mut split = decompiled_name.split('.');
        out.push_str(split.next().unwrap_or(""));
        out.push_str(".shader");

        out
    };

    // Actually decompile the material resource
    let mat_res = if is_single {
        let mut decompiled = decompile_material(ctx, &mut c)?;
        if has_shader_nodes {
            tracing::info!(
                "Material has an embedded shader; outputting shader binary to a separate file"
            );
            // Change the SJSON to say `shader = "<shader file name>"`
            decompiled.shader = Some(shader_file_name.clone());
        }

        MaterialResource::Single(decompiled)
    } else {
        MaterialResource::Set(decompile_material_set(ctx, &mut c)?)
    };

    let mut files = Vec::new();

    // Add serialized material SJSON file
    let material_sjson =
        serde_sjson::to_vec(&mat_res).wrap_err("Failed to serialize material data")?;
    files.push(UserFile::with_name(material_sjson, decompiled_name));

    // Add binary shader file if present
    if has_shader_nodes {
        // The material portion of the data is zero-padded to the nearest 16 bytes
        // Read padding and ensure it's actually 0s
        let padding_amount = (16 - (c.position() as usize) % 16) % 16;
        let mut rof_offset = (c.position() as usize) + padding_amount;

        for _ in 0..padding_amount {
            let byte = read8!(c)?;
            if byte != 0 {
                // This never happens in any material files in VT2 as of writing this
                tracing::warn!("Padding between material data and shader data is not all zeroes; outputting rest of file anyway");
                // What do we do in this situation?
                // The material resource part of the file may be fine, so output rest of material
                // file (including padding) as the shader file, even if it may be broken
                rof_offset -= padding_amount;
                break;
            }
        }

        let underlying = c.into_inner();
        let remaining_data: &[u8] = &underlying.as_ref()[rof_offset..];
        // `remaining_data` should contain a shader resource now
        // The header should start with 0x21 (meaning resource version 33)
        let shader_file = UserFile::with_name(remaining_data.to_vec(), shader_file_name);
        files.push(shader_file);
    }

    Ok(files)
}
