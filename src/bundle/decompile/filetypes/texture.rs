use std::io::{Cursor, Seek, SeekFrom};

use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use color_eyre::eyre::bail;
use color_eyre::{Report, Result};

use crate::bundle::UserFile;

const DDS_HEADER_SIZE: usize = 128;
const DX10_HEADER_SIZE: usize = 20;
const DDS_HEIGHT_OFFSET: u64 = 0xc;
const DDSPF_OFFSET: u64 = 0x50;
const DXGI_FORMAT_OFFSET: u64 = 0x80;
// ASCII of `DX10` in little endian
const FOURCC_DX10: u32 = 0x30315844;

#[derive(Copy, Clone, Debug)]
enum DDSCompressionType {
    // These are the only types of compression used by textures with streaming data
    // in VT2 as of this writing.
    Unknown,
    Bc1Unorm,
    Bc3Unorm,
    Bc4Unorm,
    Bc5Unorm,
    Bc7Unorm,
}

impl DDSCompressionType {
    fn compression_params(&self) -> (u32, u32) {
        // DDS compression works on 4x4 tiles of texels (or 1x1 if uncompressed).
        // Each tile is worth 4, 8, or 16 bytes depending on the type of compression.
        match self {
            DDSCompressionType::Unknown => (1, 4),
            DDSCompressionType::Bc1Unorm => (4, 8),
            DDSCompressionType::Bc3Unorm => (4, 16),
            DDSCompressionType::Bc4Unorm => (4, 8),
            DDSCompressionType::Bc5Unorm => (4, 16),
            DDSCompressionType::Bc7Unorm => (4, 16),
        }
    }
}

impl TryFrom<u32> for DDSCompressionType {
    type Error = Report;

    // https://learn.microsoft.com/en-us/windows/win32/api/dxgiformat/ne-dxgiformat-dxgi_format
    fn try_from(value: u32) -> Result<Self> {
        match value {
            0 => Ok(Self::Unknown),
            71 => Ok(Self::Bc1Unorm),
            77 => Ok(Self::Bc3Unorm),
            80 => Ok(Self::Bc4Unorm),
            83 => Ok(Self::Bc5Unorm),
            98 => Ok(Self::Bc7Unorm),
            _ => bail!("Unsupported DXGI_FORMAT: {}", value),
        }
    }
}

#[tracing::instrument]
fn try_find_stream_dimensions(
    stream_length: usize,
    base_height: u32,
    base_width: u32,
    compression_type: DDSCompressionType,
) -> Result<(u32, u32)> {
    if (base_height & (base_height - 1) != 0) || (base_width & (base_width - 1) != 0) {
        // Maybe there is a way to fix?
        bail!(
            "Texture dimensions {} x {} are not powers of 2",
            base_height,
            base_width
        );
    }

    let (tile_size, bytes_per_tile) = compression_type.compression_params();
    if (base_height < tile_size) || (base_width < tile_size) {
        // All textures with streaming data in VT2 have dimensions of at least 4*4, so this should never happen.
        // In DDS compression, anything smaller than a single tile still takes up the same number of bytes
        // as a tile. So if this did happen, we could resize `height` and/or `width` to meet the tile size.
        bail!(
            "Not implemented: texture dimensions {} x {} are smaller than a single tile",
            base_height,
            base_width
        );
    }

    // Add up the sizes (in bytes) of ever-increasing mipmaps until we reach the size of the
    // stream data. If the sizes match exactly, we have the dimensions of the largest mipmap.
    let stream_length = u32::try_from(stream_length)?;
    let mut mipmap_height = base_height;
    let mut mipmap_width = base_width;
    let mut sum = 0;
    while sum < stream_length {
        mipmap_height *= 2;
        mipmap_width *= 2;
        let num_tiles = (mipmap_height / tile_size) * (mipmap_width / tile_size);
        let num_bytes = num_tiles * bytes_per_tile;
        sum += num_bytes;
    }
    if sum == stream_length {
        return Ok((mipmap_height, mipmap_width));
    }

    bail!(
        "Could not determine dimensions of streamed texture data. tile_size = {}, bytes_per_tile = {}, stream_length = {}",
        tile_size,
        bytes_per_tile,
        stream_length
    );
}

#[tracing::instrument(skip(base_binary, stream_binary))]
pub fn decompile(
    base_binary: impl Into<Vec<u8>>,
    stream_binary: impl AsRef<[u8]>,
    decompiled_name: String,
) -> Result<Vec<UserFile>> {
    let base_binary = base_binary.into();
    let stream_binary = stream_binary.as_ref();

    // If a texture doesn't have stream data, it can be used as-is.
    if stream_binary.is_empty() {
        Ok(vec![UserFile::with_name(base_binary, decompiled_name)])
    } else {
        // If it does have stream data, the base texture file is the lower resolution version,
        // and the stream data contains the high resolution DDS surface data.
        // However, the stream data does not have its own header, so we have to use the header
        // from the low resolution texture, and change the image dimensions to match the high
        // resolution texture data.

        // If file is less than 128 bytes, it cannot possibly contain a valid DDS header.
        if base_binary.len() < DDS_HEADER_SIZE {
            bail!("File is too short to have a valid DDS header");
        };

        // Do we have just a DDS header, or DDS and DX10 headers?
        let mut thumbnail_csr = Cursor::new(&base_binary);
        thumbnail_csr.seek(SeekFrom::Start(DDSPF_OFFSET))?;
        let dw_flags = read32!(thumbnail_csr)?;
        let fourcc = read32!(thumbnail_csr)?;

        let is_dx10 = {
            let ddpf_fourcc = ((dw_flags >> 2) & 1) != 0;
            ddpf_fourcc && (fourcc == FOURCC_DX10)
        };

        let total_header_size: usize = if is_dx10 {
            DDS_HEADER_SIZE + DX10_HEADER_SIZE
        } else {
            DDS_HEADER_SIZE
        };

        // If there is a DX10 header, file must be at least 148 bytes to have valid headers.
        if base_binary.len() < total_header_size {
            bail!("File is too short to have a valid DDS header and DX10 header");
        };

        // Get compression format
        thumbnail_csr.seek(SeekFrom::Start(DXGI_FORMAT_OFFSET))?;
        let dxgi_format = if is_dx10 {
            let val = read32!(thumbnail_csr)?;
            DDSCompressionType::try_from(val)?
        } else {
            DDSCompressionType::Unknown
        };

        // Construct new stream DDS file
        let mut stream_dds: Vec<u8> = Vec::with_capacity(total_header_size + stream_binary.len());
        stream_dds.extend_from_slice(&base_binary[0..total_header_size]);
        stream_dds.extend(stream_binary);

        // Get base height and width
        let mut stream_csr = Cursor::new(&mut stream_dds);
        stream_csr.seek(SeekFrom::Start(DDS_HEIGHT_OFFSET))?;
        let height = read32!(stream_csr)?;
        let width = read32!(stream_csr)?;

        // Write proper height and width to new DDS
        let (stream_height, stream_width) =
            try_find_stream_dimensions(stream_binary.len(), height, width, dxgi_format)?;
        stream_csr.seek(SeekFrom::Start(DDS_HEIGHT_OFFSET))?;
        write32!(stream_csr, stream_height)?;
        write32!(stream_csr, stream_width)?;

        // Give the stream file the main name, and give the thumbnail file a "_small" suffix
        let stream_name = decompiled_name;
        let thumbnail_name = {
            let mut out = String::new();
            let mut split = stream_name.split('.');

            out.push_str(split.next().unwrap_or(""));
            out.push_str("_small");

            for s in split {
                out.push('.');
                out.push_str(s);
            }

            out
        };

        Ok(vec![
            UserFile::with_name(base_binary, thumbnail_name),
            UserFile::with_name(stream_dds, stream_name),
        ])
    }
}
