// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::fmt::Debug;

use color_eyre::Result;

use super::decompile::filetypes::*;
use crate::context::Context;
use crate::murmur::Murmur64;

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub enum BundleFileType {
    Apb,
    Bik,
    Bones,
    Chroma,
    Config,
    Crypto,
    Data,
    Entity,
    Flow,
    Font,
    Ini,
    Ivf,
    Keys,
    Level,
    Lua,
    Material,
    Mod,
    MouseCursor,
    NavData,
    NetworkConfig,
    Package,
    Particles,
    PhysicsProperties,
    RenderConfig,
    Scene,
    Shader,
    ShaderLibrary,
    ShaderLibraryGroup,
    ShadingEnvironment,
    ShadingEnvionmentMapping,
    SoundEnvironment,
    StateMachine,
    Strings,
    SurfaceProperties,
    Texture,
    TimpaniBank,
    TimpaniMaster,
    Tome,
    Unit,
    Input,
    VectorField,
    BakedLighting,
    Wav,
    Animation,
    WwiseBank,
    Slug,
    WwiseDep,
    AnimationCurves,
    WwiseMetadata,
    StaticPVS,
    WwiseStream,
    SpuJob,
    Ugg,
    Upb,
    Xml,
    Unknown(Murmur64),
}

impl BundleFileType {
    pub fn ext_name(&self) -> String {
        match self {
            BundleFileType::Apb => String::from("apb"),
            BundleFileType::Bik => String::from("bik"),
            BundleFileType::Bones => String::from("bones"),
            BundleFileType::Chroma => String::from("chroma"),
            BundleFileType::Config => String::from("config"),
            BundleFileType::Crypto => String::from("crypto"),
            BundleFileType::Data => String::from("data"),
            BundleFileType::Entity => String::from("entity"),
            BundleFileType::Flow => String::from("flow"),
            BundleFileType::Font => String::from("font"),
            BundleFileType::Ini => String::from("ini"),
            BundleFileType::Ivf => String::from("ivf"),
            BundleFileType::Keys => String::from("keys"),
            BundleFileType::Level => String::from("level"),
            BundleFileType::Lua => String::from("lua"),
            BundleFileType::Material => String::from("material"),
            BundleFileType::Mod => String::from("mod"),
            BundleFileType::MouseCursor => String::from("mouse_cursor"),
            BundleFileType::NavData => String::from("nav_data"),
            BundleFileType::NetworkConfig => String::from("network_config"),
            BundleFileType::Package => String::from("package"),
            BundleFileType::Particles => String::from("particles"),
            BundleFileType::PhysicsProperties => String::from("physics_properties"),
            BundleFileType::RenderConfig => String::from("render_config"),
            BundleFileType::Scene => String::from("scene"),
            BundleFileType::Shader => String::from("shader"),
            BundleFileType::ShaderLibrary => String::from("shader_library"),
            BundleFileType::ShaderLibraryGroup => String::from("shader_library_group"),
            BundleFileType::ShadingEnvironment => String::from("shading_environment"),
            BundleFileType::ShadingEnvionmentMapping => String::from("shading_environment_mapping"),
            BundleFileType::SoundEnvironment => String::from("sound_environment"),
            BundleFileType::StateMachine => String::from("state_machine"),
            BundleFileType::Strings => String::from("strings"),
            BundleFileType::SurfaceProperties => String::from("surface_properties"),
            BundleFileType::Texture => String::from("texture"),
            BundleFileType::TimpaniBank => String::from("timpani_bank"),
            BundleFileType::TimpaniMaster => String::from("timpani_master"),
            BundleFileType::Tome => String::from("tome"),
            BundleFileType::Unit => String::from("unit"),
            BundleFileType::VectorField => String::from("vector_field"),
            BundleFileType::Wav => String::from("wav"),
            BundleFileType::WwiseBank => String::from("wwise_bank"),
            BundleFileType::WwiseDep => String::from("wwise_dep"),
            BundleFileType::WwiseMetadata => String::from("wwise_metadata"),
            BundleFileType::WwiseStream => String::from("wwise_stream"),
            BundleFileType::Input => String::from("input"),
            BundleFileType::BakedLighting => String::from("baked_lighting"),
            BundleFileType::Animation => String::from("animation"),
            BundleFileType::Slug => String::from("slug"),
            BundleFileType::AnimationCurves => String::from("animation_curves"),
            BundleFileType::StaticPVS => String::from("static_pvs"),
            BundleFileType::SpuJob => String::from("spu_job"),
            BundleFileType::Ugg => String::from("ugg"),
            BundleFileType::Upb => String::from("upb"),
            BundleFileType::Xml => String::from("xml"),
            BundleFileType::Unknown(s) => format!("{:016X}", s),
        }
    }

    pub fn decompiled_ext_name(&self) -> String {
        match self {
            BundleFileType::Texture => String::from("dds"),
            BundleFileType::WwiseBank => String::from("bnk"),
            BundleFileType::WwiseStream => String::from("ogg"),
            _ => self.ext_name(),
        }
    }
}

impl From<u64> for BundleFileType {
    fn from(hash: u64) -> BundleFileType {
        match hash {
            0x3eed05ba83af5090 => BundleFileType::Apb,
            0xaa5965f03029fa18 => BundleFileType::Bik,
            0x18dead01056b72e9 => BundleFileType::Bones,
            0xb7893adf7567506a => BundleFileType::Chroma,
            0x82645835e6b73232 => BundleFileType::Config,
            0x69108ded1e3e634b => BundleFileType::Crypto,
            0x8fd0d44d20650b68 => BundleFileType::Data,
            0x9831ca893b0d087d => BundleFileType::Entity,
            0x92d3ee038eeb610d => BundleFileType::Flow,
            0x9efe0a916aae7880 => BundleFileType::Font,
            0xd526a27da14f1dc5 => BundleFileType::Ini,
            0xfa4a8e091a91201e => BundleFileType::Ivf,
            0xa62f9297dc969e85 => BundleFileType::Keys,
            0x2a690fd348fe9ac5 => BundleFileType::Level,
            0xa14e8dfa2cd117e2 => BundleFileType::Lua,
            0xeac0b497876adedf => BundleFileType::Material,
            0x3fcdd69156a46417 => BundleFileType::Mod,
            0xb277b11fe4a61d37 => BundleFileType::MouseCursor,
            0x169de9566953d264 => BundleFileType::NavData,
            0x3b1fa9e8f6bac374 => BundleFileType::NetworkConfig,
            0xad9c6d9ed1e5e77a => BundleFileType::Package,
            0xa8193123526fad64 => BundleFileType::Particles,
            0xbf21403a3ab0bbb1 => BundleFileType::PhysicsProperties,
            0x27862fe24795319c => BundleFileType::RenderConfig,
            0x9d0a795bfe818d19 => BundleFileType::Scene,
            0xcce8d5b5f5ae333f => BundleFileType::Shader,
            0xe5ee32a477239a93 => BundleFileType::ShaderLibrary,
            0x9e5c3cc74575aeb5 => BundleFileType::ShaderLibraryGroup,
            0xfe73c7dcff8a7ca5 => BundleFileType::ShadingEnvironment,
            0x250e0a11ac8e26f8 => BundleFileType::ShadingEnvionmentMapping,
            0xd8b27864a97ffdd7 => BundleFileType::SoundEnvironment,
            0xa486d4045106165c => BundleFileType::StateMachine,
            0x0d972bab10b40fd3 => BundleFileType::Strings,
            0xad2d3fa30d9ab394 => BundleFileType::SurfaceProperties,
            0xcd4238c6a0c69e32 => BundleFileType::Texture,
            0x99736be1fff739a4 => BundleFileType::TimpaniBank,
            0x00a3e6c59a2b9c6c => BundleFileType::TimpaniMaster,
            0x19c792357c99f49b => BundleFileType::Tome,
            0xe0a48d0be9a7453f => BundleFileType::Unit,
            0xf7505933166d6755 => BundleFileType::VectorField,
            0x786f65c00a816b19 => BundleFileType::Wav,
            0x535a7bd3e650d799 => BundleFileType::WwiseBank,
            0xaf32095c82f2b070 => BundleFileType::WwiseDep,
            0xd50a8b7e1c82b110 => BundleFileType::WwiseMetadata,
            0x504b55235d21440e => BundleFileType::WwiseStream,
            0x2bbcabe5074ade9e => BundleFileType::Input,
            0x7ffdb779b04e4ed1 => BundleFileType::BakedLighting,
            0x931e336d7646cc26 => BundleFileType::Animation,
            0xa27b4d04a9ba6f9e => BundleFileType::Slug,
            0xdcfb9e18fff13984 => BundleFileType::AnimationCurves,
            0xe3f0baa17d620321 => BundleFileType::StaticPVS,
            0xf97af9983c05b950 => BundleFileType::SpuJob,
            0x712d6e3dd1024c9c => BundleFileType::Ugg,
            0x76015845a6003765 => BundleFileType::Xml,
            0xa99510c6e86dd3c2 => BundleFileType::Upb,
            _ => BundleFileType::Unknown(hash.into()),
        }
    }
}

impl From<BundleFileType> for u64 {
    fn from(t: BundleFileType) -> u64 {
        match t {
            BundleFileType::Apb => 0x3eed05ba83af5090,
            BundleFileType::Bik => 0xaa5965f03029fa18,
            BundleFileType::Bones => 0x18dead01056b72e9,
            BundleFileType::Chroma => 0xb7893adf7567506a,
            BundleFileType::Config => 0x82645835e6b73232,
            BundleFileType::Crypto => 0x69108ded1e3e634b,
            BundleFileType::Data => 0x8fd0d44d20650b68,
            BundleFileType::Entity => 0x9831ca893b0d087d,
            BundleFileType::Flow => 0x92d3ee038eeb610d,
            BundleFileType::Font => 0x9efe0a916aae7880,
            BundleFileType::Ini => 0xd526a27da14f1dc5,
            BundleFileType::Ivf => 0xfa4a8e091a91201e,
            BundleFileType::Keys => 0xa62f9297dc969e85,
            BundleFileType::Level => 0x2a690fd348fe9ac5,
            BundleFileType::Lua => 0xa14e8dfa2cd117e2,
            BundleFileType::Material => 0xeac0b497876adedf,
            BundleFileType::Mod => 0x3fcdd69156a46417,
            BundleFileType::MouseCursor => 0xb277b11fe4a61d37,
            BundleFileType::NavData => 0x169de9566953d264,
            BundleFileType::NetworkConfig => 0x3b1fa9e8f6bac374,
            BundleFileType::Package => 0xad9c6d9ed1e5e77a,
            BundleFileType::Particles => 0xa8193123526fad64,
            BundleFileType::PhysicsProperties => 0xbf21403a3ab0bbb1,
            BundleFileType::RenderConfig => 0x27862fe24795319c,
            BundleFileType::Scene => 0x9d0a795bfe818d19,
            BundleFileType::Shader => 0xcce8d5b5f5ae333f,
            BundleFileType::ShaderLibrary => 0xe5ee32a477239a93,
            BundleFileType::ShaderLibraryGroup => 0x9e5c3cc74575aeb5,
            BundleFileType::ShadingEnvironment => 0xfe73c7dcff8a7ca5,
            BundleFileType::ShadingEnvionmentMapping => 0x250e0a11ac8e26f8,
            BundleFileType::SoundEnvironment => 0xd8b27864a97ffdd7,
            BundleFileType::StateMachine => 0xa486d4045106165c,
            BundleFileType::Strings => 0x0d972bab10b40fd3,
            BundleFileType::SurfaceProperties => 0xad2d3fa30d9ab394,
            BundleFileType::Texture => 0xcd4238c6a0c69e32,
            BundleFileType::TimpaniBank => 0x99736be1fff739a4,
            BundleFileType::TimpaniMaster => 0x00a3e6c59a2b9c6c,
            BundleFileType::Tome => 0x19c792357c99f49b,
            BundleFileType::Unit => 0xe0a48d0be9a7453f,
            BundleFileType::VectorField => 0xf7505933166d6755,
            BundleFileType::Wav => 0x786f65c00a816b19,
            BundleFileType::WwiseBank => 0x535a7bd3e650d799,
            BundleFileType::WwiseDep => 0xaf32095c82f2b070,
            BundleFileType::WwiseMetadata => 0xd50a8b7e1c82b110,
            BundleFileType::WwiseStream => 0x504b55235d21440e,
            BundleFileType::Input => 0x2bbcabe5074ade9e,
            BundleFileType::BakedLighting => 0x7ffdb779b04e4ed1,
            BundleFileType::Animation => 0x931e336d7646cc26,
            BundleFileType::Slug => 0xa27b4d04a9ba6f9e,
            BundleFileType::AnimationCurves => 0xdcfb9e18fff13984,
            BundleFileType::StaticPVS => 0xe3f0baa17d620321,
            BundleFileType::SpuJob => 0xf97af9983c05b950,
            BundleFileType::Ugg => 0x712d6e3dd1024c9c,
            BundleFileType::Xml => 0x76015845a6003765,
            BundleFileType::Upb => 0xa99510c6e86dd3c2,
            BundleFileType::Unknown(hash) => hash.into(),
        }
    }
}

pub struct UserFile {
    data: Vec<u8>,
    name: Option<String>,
}

impl UserFile {
    pub fn new(data: Vec<u8>) -> UserFile {
        UserFile { data, name: None }
    }

    pub fn with_name(data: Vec<u8>, name: String) -> UserFile {
        UserFile {
            data,
            name: Some(name),
        }
    }

    pub fn set_name<S: AsRef<str>>(&mut self, name: S) {
        self.name = Some(name.as_ref().to_owned())
    }

    pub fn get_name(&self) -> Option<&String> {
        self.name.as_ref()
    }

    pub fn get_data(&self) -> &Vec<u8> {
        &self.data
    }
}

pub struct BundleFile {
    t: BundleFileType,
    name: String,
    hash: Murmur64,
    versions: Vec<BundleFileVersion>,
}

impl BundleFile {
    pub fn new(t: BundleFileType, hash: impl Into<Murmur64>, name: String) -> BundleFile {
        BundleFile {
            t,
            hash: hash.into(),
            name,
            versions: Vec::new(),
        }
    }

    pub fn set_versions(&mut self, versions: Vec<BundleFileVersion>) {
        self.versions = versions;
    }

    pub fn get_type(&self) -> BundleFileType {
        self.t
    }

    pub fn get_hash(&self) -> Murmur64 {
        self.hash
    }

    pub fn get_file_name(&self, lang: Option<&u32>) -> String {
        if let Some(lang) = lang {
            format!("{}.{}.{}", self.name, lang, self.t.ext_name())
        } else {
            format!("{}.{}", self.name, self.t.ext_name())
        }
    }

    pub fn get_decompiled_name(&self, lang: Option<&u32>) -> String {
        if let Some(lang) = lang {
            format!("{}.{}.{}", self.name, lang, self.t.decompiled_ext_name())
        } else {
            format!("{}.{}", self.name, self.t.decompiled_ext_name())
        }
    }

    pub fn get_decompiled(&self, ctx: &Context) -> Result<Vec<UserFile>> {
        let has_multiple_versions = self.versions.len() > 1;

        self.versions
            .iter()
            .try_fold(Vec::new(), |mut files, version| {
                let lang = if has_multiple_versions {
                    Some(&version.lang)
                } else {
                    None
                };

                let mut user_files = match self.t {
                    BundleFileType::Lua => lua::decompile(ctx, &version.data)?,
                    BundleFileType::Strings => {
                        let mut strings_files = strings::decompile(ctx, &version.data)?;
                        strings_files
                            .drain(..)
                            .map(|mut f| {
                                f.set_name(self.get_decompiled_name(lang));
                                f
                            })
                            .collect()
                    }
                    BundleFileType::Mod => mod_file::decompile(&version.data)?,
                    BundleFileType::Package => package::decompile(ctx, &version.data)?,
                    BundleFileType::NetworkConfig => network_config::decompile(ctx, &version.data)?,
                    BundleFileType::Material => {
                        material::decompile(ctx, &version.data, self.get_decompiled_name(lang))?
                    }
                    BundleFileType::WwiseBank => wwise_bank::decompile(&version.data)?,
                    BundleFileType::WwiseStream => wwise_stream::decompile(
                        ctx,
                        &version.data,
                        &version.stream_data,
                        self.get_decompiled_name(lang),
                    )?,
                    BundleFileType::WwiseDep => wwise_dep::decompile(&version.data)?,
                    BundleFileType::Texture => texture::decompile(
                        version.data.clone(),
                        &version.stream_data,
                        self.get_decompiled_name(lang),
                    )?,
                    BundleFileType::Bones => bones::decompile(&version.data)?,
                    _ => {
                        tracing::debug!(
                            "Can't decompile '{}'. Unknown file type.",
                            &self.get_decompiled_name(lang)
                        );
                        vec![UserFile::with_name(
                            version.data.clone(),
                            self.get_decompiled_name(lang),
                        )]
                    }
                };

                files.append(&mut user_files);

                Ok(files)
            })
    }

    pub fn get_raw(&self) -> Result<Vec<UserFile>> {
        let has_multiple_versions = self.versions.len() > 1;

        let files = self.versions.iter().fold(Vec::new(), |mut files, v| {
            let lang = if has_multiple_versions {
                Some(&v.lang)
            } else {
                None
            };

            files.push(UserFile::with_name(
                v.data.clone(),
                self.get_file_name(lang),
            ));

            if !v.stream_data.is_empty() {
                let stream_data = v.stream_data.clone();
                files.push(UserFile::with_name(
                    stream_data,
                    format!("{}.stream", self.name),
                ));
            }

            files
        });

        Ok(files)
    }

    pub fn get_size(&self) -> usize {
        self.versions
            .iter()
            .fold(0, |size, version| size + version.data.len())
    }

    pub fn get_stream_size(&self) -> usize {
        self.versions
            .iter()
            .fold(0, |size, version| size + version.stream_data.len())
    }
}

#[derive(Default)]
pub struct BundleFileVersion {
    // There is only one known usage in VT2, in which case this is used
    // as a bit field. But that is weird, since a bitfield suggest
    // permutations where more than one field is set, but the actual usage
    // is something where each version is mutualy exclusive.
    pub lang: u32,
    pub data: Vec<u8>,
    pub stream_data: Vec<u8>,
}
