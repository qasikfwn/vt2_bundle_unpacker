// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Currently unused
// mod murmurhash32;
mod dictionary;
mod murmurhash64;
mod rainbow_table;
mod types;

pub const SEED: u32 = 0;

pub use dictionary::{Dictionary, HashGroup};
pub use murmurhash64::hash_inverse as inverse;
pub use murmurhash64::hash_inverse32 as inverse32;
pub use murmurhash64::{hash, hash32, MurmurBuildHasher};
pub use rainbow_table::RainbowTable;
pub use types::*;

