// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#![forbid(unsafe_code)]

use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

use clap::{Parser, Subcommand};
use color_eyre::eyre::Context as _;
use color_eyre::Result;

use unpacker::murmur::Dictionary;
use unpacker::Context;

mod cmd {
    pub mod decompress;
    pub mod experiment;
    pub mod extract;
    pub mod list;
    pub mod log;
    pub mod murmur;
    mod util;
}

#[derive(Parser)]
#[command(version, about)]
struct Cli {
    /// Specify multiple times to increase the log level.
    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,

    /// Add debug data to log output
    #[arg(long, value_name = "bool")]
    debug: Option<bool>,

    /// A CSV file containing a dictionary of known strings and their hashes.
    /// An additional 'group' field allos separating strings by their usage
    /// within the game data.
    #[arg(long, value_name = "FILE", default_value = "dictionary.csv")]
    dict: PathBuf,

    /// DEPRECATED! See '--dict'.
    /// A file containing a list of strings to seed the hash rainbow table with.
    #[arg(long = "seed", value_name = "FILE")]
    seed_files: Vec<PathBuf>,

    /// With the Oktober 2023 update, the engine now uses a different compression
    /// algorithm for the bundles, which uses a shared compression dictionary,
    /// located in the game's `bundle/` directory.
    /// By default, 'unpacker' will look for it based on the given bundle path.
    /// So if you're operating on bundles directly from the game's directory,
    /// it will "just work".
    /// In any other case, you will have to specify the path to the
    /// `compression.dictionary` file that corresponds to the bundle
    /// you're trying to open.
    #[arg(long, value_name = "FILE")]
    zstd_dict: Option<PathBuf>,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    Decompress(cmd::decompress::Command),
    Experiment(cmd::experiment::Command),
    Extract(cmd::extract::Command),
    List(cmd::list::Command),
    Murmur(cmd::murmur::Command),
}

#[tracing::instrument]
fn main() -> Result<()> {
    let cli = Cli::try_parse()?;

    cmd::log::create_tracing_subscriber(cli.verbose, cli.debug);

    let mut dictionary = match File::open(&cli.dict) {
        Ok(f) => {
            tracing::debug!("Reading dictionary file '{}'.", cli.dict.display());

            let buf_reader = BufReader::new(f);
            let mut dictionary = Dictionary::new();
            dictionary.read_csv(buf_reader).wrap_err_with(|| {
                format!("Failed to parse dictionary file '{}'", cli.dict.display())
            })?;

            dictionary
        }
        Err(e) => {
            tracing::error!(
                "Failed to read dictionary file '{}': {}",
                cli.dict.display(),
                e
            );
            Dictionary::new()
        }
    };

    if !cli.seed_files.is_empty() {
        tracing::warn!(
            "Usage of '--seed' is deprecated.  \
            Please convert your files to the new dictionary format and use '--dict' \
            or download a new one."
        );

        for seed_file in cli.seed_files {
            tracing::debug!("Reading seed file '{}'.", seed_file.display());
            let f = File::open(&seed_file)
                .wrap_err_with(|| format!("Failed to read seed file '{}'", seed_file.display()))?;

            let buf_reader = BufReader::new(f);
            dictionary.read_seed_file(buf_reader)?;
        }
    }

    let compression_dictionary = cli.zstd_dict.or_else(|| {
        let bundle_path = match &cli.command {
            Commands::Decompress(opts) => Some(opts.get_bundle_path()),
            Commands::Extract(opts) => Some(opts.get_bundle_path()),
            Commands::List(opts) => Some(opts.get_bundle_path()),
            _ => None,
        };
        bundle_path
            .and_then(|p| p.parent())
            .map(|parent| parent.join("compression.dictionary"))
    });

    let ctx = Context {
        dictionary,
        verbosity: cli.verbose,
        compression_dictionary,
        ljd: None,
        revorb: None,
        ww2ogg: None,
    };

    match cli.command {
        Commands::List(matches) => cmd::list::run(ctx, matches),
        Commands::Extract(matches) => cmd::extract::run(ctx, matches),
        Commands::Decompress(matches) => cmd::decompress::run(ctx, matches),
        Commands::Murmur(matches) => cmd::murmur::run(ctx, matches),
        Commands::Experiment(matches) => cmd::experiment::run(ctx, matches),
    }
}
