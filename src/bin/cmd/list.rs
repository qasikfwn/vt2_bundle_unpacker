// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::path::PathBuf;

use clap::Args;
use color_eyre::eyre::Context as _;
use color_eyre::Result;
use pretty_bytes::converter::convert as pretty_bytes;

use super::util;
use unpacker::{self, Context};

/// Extract bundle files
#[derive(Args, Debug)]
pub struct Command {
    /// Read the provided bundle without applying patch files.
    /// Can be used to read a patch bundle.
    #[arg(long)]
    skip_patches: bool,

    /// the bundle file to read
    bundle: PathBuf,
}

impl Command {
    #[inline]
    pub fn get_bundle_path(&self) -> &PathBuf {
        &self.bundle
    }
}

#[tracing::instrument(skip(ctx), fields(%ctx))]
pub fn run(ctx: Context, matches: Command) -> Result<()> {
    let bundle_path = matches.bundle;

    let patches = util::get_patch_paths(&bundle_path, matches.skip_patches)
        .wrap_err("Failed to read patch paths")?;

    let bundle = unpacker::read_bundle(&ctx, &bundle_path, patches).wrap_err_with(|| {
        format!(
            "failed to load bundle data from '{}'",
            bundle_path.display()
        )
    })?;

    for file in bundle.get_files() {
        let name = file.get_file_name(None);
        let size = file.get_size();
        let stream_size = file.get_stream_size();
        println!(
            "{} ({}) ({})",
            name,
            pretty_bytes(size as f64),
            pretty_bytes(stream_size as f64)
        );
    }

    Ok(())
}
