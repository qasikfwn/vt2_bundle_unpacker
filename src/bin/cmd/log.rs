use tracing::Metadata;
use tracing_error::ErrorLayer;
use tracing_subscriber::filter::FilterFn;
use tracing_subscriber::fmt;
use tracing_subscriber::prelude::*;
use tracing_subscriber::EnvFilter;

pub fn filter_fields(metadata: &Metadata<'_>) -> bool {
    metadata
        .fields()
        .iter()
        .any(|field| field.name() == "message")
}

pub(crate) fn create_tracing_subscriber(verbosity: u8, debug_data: Option<bool>) {
    let env_layer = match verbosity {
        1 => "debug".into(),
        2 => "trace".into(),
        _ => EnvFilter::from_default_env(),
    };

    let (dev_layer, prod_layer, filter_layer) =
        if debug_data == Some(true) || (debug_data.is_none() && cfg!(debug_assertions)) {
            (Some(fmt::layer().pretty().without_time()), None, None)
        } else {
            (
                None,
                Some(fmt::layer().compact().without_time()),
                Some(FilterFn::new(filter_fields)),
            )
        };

    tracing_subscriber::registry()
        .with(filter_layer)
        .with(env_layer)
        .with(dev_layer)
        .with(prod_layer)
        .with(ErrorLayer::new(fmt::format::Pretty::default()))
        .init();
}
