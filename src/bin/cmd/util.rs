// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::path::{Path, PathBuf};

use color_eyre::eyre::Context as _;
use color_eyre::{Report, Result};
use glob::glob;

pub fn get_patch_paths(bundle_path: impl AsRef<Path>, skip_patches: bool) -> Result<Vec<PathBuf>> {
    let bundle_path = bundle_path.as_ref();

    if skip_patches {
        tracing::debug!("Loading bundle {} without patches", bundle_path.display());

        return Ok(Vec::new());
    }

    let mut paths = glob(&format!("{}.patch_*", bundle_path.display()))?;

    let patches = paths.try_fold(Vec::new(), |mut paths, path| {
        let path = path.wrap_err("invalid path when globbing")?;

        // Skip `.stream` files, as they are not valid patches.
        // We can safely unwrap here, as the glob pattern guarantees
        // a file extension
        if path.extension().unwrap() == "stream" {
            tracing::debug!("found stream when globbing for patches: {}", path.display());
            return Ok(paths);
        }

        paths.push(path);
        Ok::<_, Report>(paths)
    })?;

    tracing::debug!("Loading bundle {} with patches", bundle_path.display());

    if tracing::enabled!(tracing::Level::TRACE) {
        if !patches.is_empty() {
            for patch in &patches {
                tracing::trace!("Loading patch {}", patch.display());
            }
        } else {
            tracing::trace!("No patch files to load");
        }
    }

    Ok(patches)
}
