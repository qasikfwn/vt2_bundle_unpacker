// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::fs::{self, File};
use std::path::PathBuf;

use byteorder::{LittleEndian, ReadBytesExt};
use clap::Args;
use color_eyre::eyre::Context as _;
use color_eyre::Result;
use unpacker::{decompress, BundleFormat, Context};

/// Decompress (inflate) raw bundle data
#[derive(Args, Debug)]
pub struct Command {
    /// The bundle file to read
    bundle: PathBuf,

    /// The file to write to
    destination: PathBuf,
}

impl Command {
    #[inline]
    pub fn get_bundle_path(&self) -> &PathBuf {
        &self.bundle
    }
}

#[tracing::instrument(skip(ctx), fields(%ctx))]
pub fn run(ctx: Context, matches: Command) -> Result<()> {
    let mut f = File::open(&matches.bundle)?;
    let format = f.read_u32::<LittleEndian>()?;
    let format = BundleFormat::try_from(format)?;

    let data = decompress(&ctx, &mut f, format)?;
    fs::write(&matches.destination, data).wrap_err_with(|| {
        format!(
            "failed to write decompressed bundle '{}' to '{}'",
            matches.bundle.display(),
            matches.destination.display()
        )
    })?;

    Ok(())
}
