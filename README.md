# Bundle Unpacker for Warhammer Vermintide 2 (Bitsquid Engine)

## Download

Download the latest [release](https://gitlab.com/lschwiderski/vt2_bundle_unpacker/-/releases) for your platform. Download the [dictionary files](https://gitlab.com/lschwiderski/vt2_bundle_unpacker/-/snippets/2000115) as well and use the `--seed` [global option](https://gitlab.com/lschwiderski/vt2_bundle_unpacker/-/wikis/cli_reference#user-content-global-options) to allow the tool to lookup known values of hashed names.

## Running

To run a debug version with on-demand building, use cargo run. Otherwise, build and run the executable as above.
The general usage is:

```shell
unpacker [global_options] <subcommand> [command_options] [command_arguments]
```

A list of subcommands and options can be seen with `unpacker --help` or [in the CLI Reference](https://gitlab.com/lschwiderski/vt2_bundle_unpacker/-/wikis/cli_reference).

### Runtime Dependencies

The decompilation of certain file types requires external tools.
These will be called from `PATH` by default, but custom executable paths may be passed as arguments (see [the CLI reference](https://gitlab.com/lschwiderski/vt2_bundle_unpacker/-/wikis/cli_reference#user-content-extract).

#### Lua (`.lua`)

The LuaJit decompiler (short "ljd") is used to decompile Lua files.
A version tailored specifically to VT2 lua sources may be found here: <https://github.com/Aussiemon/ljd>.

A custom executable location may be passed via the `--ljd` flag,
otherwise decompilation expects `ljd` to exist in `PATH`.

#### Sound (`.wwise_stream`)

`ww2ogg` and `revorb` are required to decompile these sound files.
These can be found here:

- ww2ogg: [Compiled Windows](https://github.com/hcs64/ww2ogg/releases/latest), [Source Any](https://github.com/hcs64/ww2ogg)
- revorb: [Compiled Windows](http://yirkha.fud.cz/progs/foobar2000/revorb.exe), [Source *nix](https://github.com/jonboydell/revorb-nix/)

`ww2ogg` needs to be wrapped in a custom script to handle the required `--pcb` flag:

Example *nix:
```shell
#!/bin/sh

/path/to/ww2ogg/ww2ogg --pcb /path/to/ww2ogg/packed_cookbook_aoTuV_603.bin "$@"
```

Custom executable locations can be passed via the `--ww2ogg` and `--revorb` flags,
otherwise decompilation expects them to exist in `PATH` under their respective names.

## Contributing

[Bug reports](https://gitlab.com/lschwiderski/vt2_bundle_unpacker/-/issues)
and [merge request](https://gitlab.com/lschwiderski/vt2_bundle_unpacker/-/merge_requests)
are welcome.

Discussions and code sharing also welcome in the mailing list
[vt2unpacker@lists.sclu1034.dev](mailto:vt2unpacker@lists.sclu1034.dev)
(archived at
[lists.sclu1034.dev](https://lists.sclu1034.dev/hyperkitty/list/vt2unpacker@lists.sclu1034.dev/)).

### Building

Build the project with one of the following commands.

```shell
# Binary with debug symbols for useful stack traces
cargo build

# Optimized binary
cargo build --release
```

The binary will be written to `target/{debug,release}/unpacker`.

#### Container Build

Useful on host system where Rust is not installed.

```shell
docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/myapp -w /usr/src/myapp "rust:latest" cargo build --release
```

## Credits

The pipeline to extract Wwise Banks into `.wav` is largely based on eXpl0it3r's
[bnkextr](https://github.com/eXpl0it3r/bnkextr/) and Zwagoth "zabb65" Klaar's
[wwise_ima_adpcm](https://bitbucket.org/zabb65/payday-2-modding-information/src/default/sound%20tools/wwise_ima_adpcm/)
([Bitbucket killed the link](https://archive.org/details/archiveteam_bitbucket)
and I forgot to archive.org it, so the sources are gone.
You might still find compiled versions floating around in various modding communities, though).
